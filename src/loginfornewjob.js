const Joi = require('joi');
import uploadFileToS3  from "./utils/uploadFileToS3"
const UserModel = require('../models/users')
const jobsModel = require('../models/postjob');
const ServiceModel = require('../models/service')
const ResumeModel = require('../models/postresume')

const routes = [    
{
    method:'POST',
    path:'/new-form-submition',
    config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user",
        validate: {
         	payload:{
         		emailid:Joi.string(),
         		password:Joi.string().required(),

			}
		},
    },
    handler: function(request, reply){
        let session = request.state.session
        var query = ({$or:[{emailid: request.payload.emailid},{mobile: request.payload.emailid}]}, {$and:[{password: request.payload.password}]})
        UserModel.findOne(query, function(err, data){
            if (err){
                reply({
                    'error': err
                });
            } else if (!data){
	            reply.view('error', {message: 'User dose not exists please try with your correct email and password', errormessage: '404 error'})
	        } 
            else if (data.Status == "SuperAdmin") {
                if (request.state.emailid.newJobs) {
                    let newJobs = new UserModel(request.state.emailid.newJobs);
                    newJobs.save()
                }
            	let emailid = request.payload.emailid;
            	const token = jwt.sign({
            		emailid,
            		userid:data['_id'],
            	},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
            		algorithm: 'HS256',
            		expiresIn: '1h',
            	});
            	reply.state('emailid', emailid)
            	request.cookieAuth.set({ token });
            	return reply.redirect('/admin/deshboard')
            }else if (data.Status == "User") {
                if (request.state.emailid == undefined) {
                    request.cookieAuth.set(data);
                    return reply.redirect('/user/deshboard').unstate('emailid')
                }else if (request.state.emailid.newJobs) {
                    let newJob = request.state.emailid.newJobs;
                    let newJobs = new jobsModel(newJob);
                    newJobs.objectID = request.payload.emailid;
                    newJobs.save()
                    .then(function(response){
                        request.cookieAuth.set(data);
                        return reply.redirect('/user/deshboard').unstate('emailid')
                    });
                }else if (request.state.emailid.newService) {
                    let newSer = request.state.emailid.newService;
                    let newServices = new ServiceModel(newSer);
                    newServices.userobjectid = request.payload.emailid;
                    newServices.save()
                    .then(function(response){
                        console.log(data)
                        request.cookieAuth.set(data);
                        return reply.redirect('/make/payment').unstate('emailid') 
                    });
                }else if (request.state.emailid.newResume) {
                    let newRes = request.state.emailid.newResume;
                    let newResumes = new ResumeModel(newRes);
                    newResumes.objectID = request.payload.emailid;
                    newResumes.save()
                    .then(function(response){
                        request.cookieAuth.set(data);
                        return reply.redirect('/user/deshboard').unstate('emailid')
                    });
                }
                else{
                    request.cookieAuth.set(data);
                    return reply.redirect('/user/deshboard').unstate('emailid')
                }
            }else{
	            reply.view('error', {message: 'User dose not exists please try with your correct email and password', errormessage: '404 error'})
            }
        });

    }
},
{
    method: 'POST',
    path: '/tesi',
    handler: function(request, reply){
        var query = ({$or:[{emailid: request.payload.user},{mobile: request.payload.user}]}, {$and:[{password: request.payload.password}]})

        UserModel.findOne(query)
        .then(function(res){
            if (!res) {
                return reply('user dose not exists')
            }else{
                return reply(res)
            }
        });
    }
},
{
    method: 'POST',
    path: '/test',
    handler: (request, reply) => {
        let file =  request.payload.file;
        console.log(request.payload.file)
        uploadFileToS3(file, "merakaamkaajCat", "image",  'kaamkaajcom')
            .then(({ fileLink }) => {
                console.log(fileLink);
                return reply(fileLink);
            })
    }
},
{
    method: 'GET',
    path: '/html',
    handler: (request, reply) => {
        return reply.view('text')
    }
},
]
export default routes;
