'use strict'

import axios from 'axios';
import jwt from 'jsonwebtoken';

// import bson.objectid from 'ObjectId';

const db = require('../database').db;
const Joi = require('joi');
const fs = require('fs');
const path = require('path')
const jobsModel = require('../models/postjob');
const ServiceModel = require('../models/service')
const ResumeModel = require('../models/postresume')
const UserModel = require('../models/users')
const swal = require('../node_modules/sweetalert')
const nodemailer = require("nodemailer");
const localStorage = require('node-localstorage')
const opn = require('opn');
const JobCategoryModel = require('../models/addjobcategory')
const ServiceModel1 = require('../models/addservice')
const AuthCookie = require('hapi-auth-cookie')
const async = require('async');
var ObjectID = require('mongodb').ObjectID
import uploadFileToS3  from "./utils/uploadFileToS3"
import uploadResumeToS3  from "./utils/uploadResumeToS3"

// var MongoDataTable = require('mongo-datatable');





const routes = [
{
    method:'POST',
    path:'/android/user/login',
    config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"android api",
        notes:"User can login",
        validate: {
         	payload:{
         		Username:Joi.string().required(),
         		password:Joi.string().required(),

			}
		},
    },
    handler: function(request, reply){
    	UserModel.findOne({emailid: request.payload.Username, password: request.payload.password, isVerified: true})
    	.then(function(response){
    		if (!response) {
    			UserModel.findOne({mobile: request.payload.Username, password: request.payload.password, isVerified: true})
    			.then(function(data){
    				if (!data) {
    					return reply({
    						StatusCode: 400,
    						message: 'user not exists'
    					})
    				}else{
    					const Username = request.payload.Username;
				    	const token = jwt.sign({
				    		Username,
				    		userid:data['_id'],
				    	},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
				    		algorithm: 'HS256',
				    		expiresIn: '1h',
				    	});
    					return reply({
    						StatusCode: 200,
    						data: data,
    						token: token
    					});
    				}
    			})
    		}else{
    			const Username = request.payload.Username;
		    	const token = jwt.sign({
		    		Username,
		    		userid:response['_id'],
		    	},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
		    		algorithm: 'HS256',
		    		expiresIn: '1h',
		    	});
    			return reply({
    				StatusCode: 200,
					data: response,
					token: token
    			});
    		}
    	});
    }
},
{
		method: 'GET',
		path: '/android/user/deshboard',
		config:{
	    //include this route in swagger documentation
	    tags:['api'],
	    description:"getting details of a particular user",
	    notes:"getting details of particular user",
	    // auth:{
	    // 	strategy: 'restricted',
	    // }
	},
	handler:(request, h)=>{
  		let authenticated_user = request.state;
  		return h(authenticated_user)
        // UserModel.findOne({emailid: email_id}, function(err, user){
        // 	if (err) {
        // 		throw err
        // 	}else{
        // 		return h(user)
        		
        // 	}
        // });
	}

},
{
	method: 'POST',
	path: '/android/registere/user',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"verifing phone number",
        notes:"verifing phone number",
        validate: {
         	payload:{
         		emailid:Joi.string().required(),
         		firstname: Joi.string().required(),
				lastname: Joi.string().required(),
				mobile: Joi.string().required(),
				emailid: Joi.string().required(),
				password: Joi.string().required(),
				address: Joi.string().required(),
				state: Joi.string().required(),
				city: Joi.string().required(),
				pincode: Joi.number().required(),
				gender: Joi.string().required(),
				lookingfor: Joi.string().required(),
			}
		},
    },
	handler: function(request, reply){
		var otp = Math.floor(Math.random() * 90000) + 10000;
		var newUser = new UserModel({
			firstname: request.payload.firstname,
			lastname: request.payload.lastname,
			mobile: request.payload.mobile,
			emailid: request.payload.emailid,
			password: request.payload.password,
			address: request.payload.address,
			state: request.payload.state,
			city: request.payload.city,
			pincode: request.payload.pincode,
			gender: request.payload.gender,
			lookingfor: request.payload.lookingfor,
			Status: "User",
			otp: otp
		});
		UserModel.findOne({'mobile': request.payload.mobile}, function(err, data){
			if (!data) {
				let otpmessage = 'merakaamkaaj.com \n Your OTP is:'+otp
				axios.request('http://zapsms.co.in/vendorsms/pushsms.aspx?user=merakaamkaaj&password=merakaamkaaj&msisdn='+request.payload.mobile+'&sid=MERAKK&msg='+otpmessage+'&fl=1&gwid=2')
				.then(function(response){
					newUser.save(function(err, data){
						if (err) {
							reply({
								StatusCode: 404,
								message: err,
							})
						}else{
							return reply({
								StatusCode: 200,
								message: "We've sent an OTP to "+request.payload.mobile+". If this is a valid Phone Number, you should receive an OTP within the next few minutes.",
								data: newUser,
							});
						}
					})
				})
				.catch(error => {
					console.log(error);
				});
			}else{
				return reply({
					StatusCode: 404,
					message: 'User Already Exists Please Try With Another Email or Phone Number'
				})
			}
		});
	}
},
{
    method:'POST',
    path:'/android/verifing/phone/number',
    config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"verifing phone number",
        notes:"verifing phone number and important phone number should be number",
        validate:{
        	payload:{
        		_id: Joi.string().required(),
        		otp: Joi.string().required()
        	}
        }
    },
    handler: function(request, reply){
    	let updateVerify = { $set: 
    		{
				isVerified: true,
			}
		};
		UserModel.findOne({_id: ObjectID(request.payload._id), otp: request.payload.otp}, function(err, data){
			if (!data) {
				return reply({
	    			StatusCode: 404,
	    			message: "Wrong OTP Please Try With Correct OTP PIN if you Didn't Get Any Code Please Go Back And Try Again"
	    		});
			}else{
				UserModel.findOneAndUpdate({_id: ObjectID(request.payload._id), otp: request.payload.otp}, updateVerify, function(err, data1){
					if (err) {
						return reply({
			    			StatusCode: 403,
			    			message: err
			    		});
					}else{
						let success = 'Registered Successfully Thanks To Be A Member Of Merakaamkaaj.com your ID: '+data.mobile+' Your Password: '+data.password+''
		    			axios.request('http://zapsms.co.in/vendorsms/pushsms.aspx?user=merakaamkaaj&password=merakaamkaaj&msisdn='+data.mobile+'&sid=MERAKK&msg='+success+'&fl=0&gwid=2')
		    			.then(function(res){
		    				return reply({
		    					errormessage: 200,
		    					message:'Your Profile Has Successfully Made By Mera Kaam kaaj Please Login First',
		    				});
		    			});
					}
				})
			}
		});
    }
},
{
        method:'POST',
        path:'/android/search/jobs',
        config:{
            //include this route in swagger documentation
            tags:['api'],
            description:"search Jobs",
            notes:"for next 10 data you have to put only count on query after url",
            validate:{
                payload:{
                    jobType:Joi.string(),
                    state:Joi.string()
                }
            },
        },
        handler: function(request, reply){
            var query = {$and:[{verifi: 'Active'}, {jobType:{$regex: request.payload.jobType, $options: 'i'}},{state:{$regex: request.payload.state, $options: 'i'}}]}
            
            jobsModel.find(query).limit(10).skip(10 * request.query.count)
            .then(function(response){
            	return reply({
					StatusCode: 200,
					message: 'success',
					data: response
				});   	
            })
        }
    },
    {
        method:'POST',
        path:'/android/search/service',
        config:{
            //include this route in swagger documentation
            tags:['api'],
            description:"search service",
            notes:"for next 10 data you have to put only count on query after url",
            validate:{
                payload:{
                    TypeOfService:Joi.string(),
                    State:Joi.string()
                }
            },
        },
        handler: function(request, reply){
            var query = {$and:[{verifi: 'Active'}, {TypeOfService:{$regex: request.payload.TypeOfService, $options: 'i'}},{State:{$regex: request.payload.State, $options: 'i'}}]}
            ServiceModel.find(query).limit(10).skip(10 * request.query.count)
            .then(function(response){
	        	return reply({
					StatusCode: 200,
					message: 'success',
					data: response
				});
            })
        }
    },
{
	method: 'GET',
	path: '/android/our/workers',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting workers",
         notes:"in this route we are getting all workers"
     },
	handler: (request, reply) =>{
		JobCategoryModel.find({}, (err, allJobCategory) =>{
			if (err) {
				
				throw err;
			}else{
				reply({
					messageCode: 200,
					message: 'success',
					data: allJobCategory
				});
			}
		});   
	}
},
{
	method: 'GET',
	path: '/android/our/services',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting jobs",
         notes:"in this route we are getting all jobs"
     },
	handler: (request, reply) =>{
		ServiceModel1.find({}, function(err, data){
			if (err) {
				
				throw err;
			}else{
				reply({
					StatusCode: 200,
					message: 'success',
					data: data
				});
			}
		}); 
	}
},
{
	method: 'GET',
	path: '/android/recent/services',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting jobs",
         notes:"in this route we are getting all jobs"
     },
	handler: (request, reply) =>{
		// var recentsers = {};	
		// var recentjobs = {};
		ServiceModel.find().limit(10).exec({}, (err, data) =>{
			if (err) {
				
				throw err;
			}else{
				reply({
					StatusCode: 200,
					message: 'success',
					data: data
				});
			}
		})    
	}
},
{
	method: 'GET',
	path: '/android/recent/right/job',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting 5 jobs and getting 5 workers",
         notes:"in this route we are getting jobs and workers"
     },
	handler: (request, reply) =>{	
		jobsModel.find({verifi: 'Active'}).limit(5).exec({}, (err, recentworker) =>{
			return reply({
				StatusCode: 200,
				message: 'success',
				data: recentworker
			})
		});
	}   
},
{
	method: 'GET',
	path: '/android/recent/right/worker',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting 5 jobs and getting 5 workers",
         notes:"in this route we are getting jobs and workers"
     },
	handler: (request, reply) =>{	
		ResumeModel.find({verifi: 'Active'}).limit(5).exec({}, (err, recentjob) =>{
			return reply({
				StatusCode: 200,
				message: 'success',
				data: recentjob
			})
		});
	}   
},
// ==============================================
// POST JOB POST RESUME POST SERVICE
{
	method: 'POST',
	path: '/android/post/service/images',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"post service images",
         notes:"post services images",
     },
	handler: function(request, h){
		let file =  request.payload.file;
    	uploadFileToS3(file, "ServiceImageUrl", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		return h({
    			StatusCode: 200,
    			message: 'success',
    			awsImageUrl: fileLink 
    		})
    	});
	}
},
{
	method: 'POST',
	path: '/android/post/service-user',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting service tamplate",
         notes:"in this route we are getting service tamplate",
         validate:{
            payload:{
            	"userobjectid": Joi.string(),
			    "serviceid": "0",
			    "verifi": "In Active",
			    "TypeOfService": Joi.string(),
				"Specification": Joi.string(),
				"ProvideServices": Joi.string(),
				"ProviderRegistered": Joi.string(),
				"RegisteredExpiry": Joi.string(), 
				"Country": Joi.string(),
				"State": Joi.string(),
				"City": Joi.string(),
				"Area": Joi.string(),

				// Provier Details
				"Agency": Joi.string(),
				"Representative": Joi.string(),
				"MobileNumber": Joi.number(),
				"LandNumber": Joi.string(),
				"Timing": Joi.string(),
				"aadharcard": Joi.string(),
				"website": Joi.string(),
				"emailMobile": Joi.string(),
				"address": Joi.string(),
				"pincode": Joi.number(),
				"information": Joi.string(),
				"payment": Joi.string(),
				"termsandcondition": Joi.boolean(),
				"awsImageUrl": Joi.string()
            }
        },
     },
	handler: (request, reply) =>{
		var newService = new ServiceModel({
			"userobjectid": request.payload.userobjectid,
		    "serviceid": "0",
		    "verifi": "In Active",
		    "TypeOfService": request.payload.TypeOfService,
			"Specification": request.payload.Specification,
			"ProvideServices": request.payload.ProvideServices,
			"ProviderRegistered": request.payload.ProviderRegistered,
			"RegisteredExpiry": request.payload.RegisteredExpiry, 
			"Country": request.payload.Country,
			"State": request.payload.State,
			"City": request.payload.City,
			"Area": request.payload.Area,

			// Provier Details
			"Agency": request.payload.Agency,
			"Representative": request.payload.Representative,
			"MobileNumber": request.payload.MobileNumber,
			"LandNumber": request.payload.LandNumber,
			"Timing": request.payload.Timing,
			"aadharcard": request.payload.aadharcard,
			"website": request.payload.website,
			"emailMobile": request.payload.emailMobile,
			"address": request.payload.address,
			"pincode": request.payload.pincode,
			"information": request.payload.information,
			"payment": request.payload.payment,
			"termsandcondition": request.payload.termsandcondition,
			"awsImageUrl": request.payload.awsImageUrl
		});
		newService.save()
		.then(function(response){
			console.log(response)
			return reply({
				StatusCode: 200,
				message: 'success',
				data: response
			});
		});
	}
},
{
	method: 'POST',
	path: '/android/post/resume/images',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"post resume images",
         notes:"post resume images",
     },
	handler: function(request, h){
		let file =  request.payload.file;
    	uploadFileToS3(file, "ResumeImageUrl", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		return h({
    			StatusCode: 200,
    			message: 'success',
    			awsImageUrl: fileLink 
    		})
    	});
	}
},
{
	method: 'POST',
	path: '/android/post/resume/file',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"post resume",
         notes:"post resume",
     },
	handler: function(request, h){
		let file =  request.payload.file;
    	uploadResumeToS3(file, "ResumeImageUrl", "image",  'kaamkaajcom')
		.then(({ fileLink }) => {
			return h({
    			StatusCode: 200,
    			message: 'success',
    			awsResumeLink: fileLink 
    		})
		});
	}
},
{
	method: 'POST',
	path: '/android/post/resume-user',
	config: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Posting resume",
         notes:"in this route we can post resume",
         validate:{
            payload:{
            	"objectID": Joi.string(),
				"JobCat": Joi.string(),
				"Name":Joi.string(),
				"Mobile": Joi.number(),
				"AlternateNum":Joi.number(),
				"EmailorMobile": Joi.string(), 
				"AdharNo":Joi.string(),
				"Country":Joi.string(),
				"State":Joi.string(),
				"City":Joi.string(),
				"Pincode":Joi.number(),
				"Address":Joi.string(),
				//About My Self
				"Gender":Joi.string(),
				"DOB":Joi.string(),
				"Religin":Joi.string(),
				"knownLanguage":Joi.string(),
				//Education Details
				"Class":Joi.string(),
				"Degree":Joi.string(),
				"PG":Joi.string(),
				"Diploma":Joi.string(),
				"Course":Joi.string(),
				"CareerObjective":Joi.string(),
				"OtherDetails":Joi.string(),
				// OtherDetails
				"ReferenceName":Joi.string(),
				"ReferenceMobile":Joi.string(),
				// Looking Overseas?
				"LookingOverseas": Joi.string(),
				// Please Tick If do you have current Work Details
				"NameofImpoloyer":Joi.string(),
				"PositionDesignation":Joi.string(),
				"MainJobCategory":Joi.string(),
				"Skills":Joi.string(),
				"Experience":Joi.string(),
				"currentSalary":Joi.number(),
				"ExpSalary":Joi.number(),
				"PriferredShift":Joi.string(),
				"PriferredJobDescription":Joi.string(),
				"PriferredLocation":Joi.string(),
				// If do you have past work Details
				"NameofImpoloyer1":Joi.string(),
				"PositionDesignation1":Joi.string(),
				"JobCategory1":Joi.string(),
				"State1City1":Joi.string(),
				"Experience1":Joi.string(),
				"shift1":Joi.string(),
				"jobDescription1":Joi.string(),
				"SalaryWithdrawn":Joi.number(),
				"termsandcondition": Joi.boolean(),
				"awsImageUrl": Joi.string(),
			    "awsResumeUrl": Joi.string()
            }
        },
	},
	handler: (request, reply) => {
		var newResume = new ResumeModel({
			"objectID": request.payload.objectID,
			"pwid": "0", 
			"verifi": "In Active",
			"JobCat": request.payload.JobCat,
			"Name":request.payload.Name,
			"Mobile": request.payload.Mobile,
			"AlternateNum":request.payload.AlternateNum,
			"EmailorMobile": request.payload.EmailorMobile, 
			"AdharNo":request.payload.AdharNo,
			"Country":request.payload.Country,
			"State":request.payload.State,
			"City":request.payload.City,
			"Pincode":request.payload.Pincode,
			"Address":request.payload.Address,
			//About My Self
			"Gender":request.payload.Gender,
			"DOB":request.payload.DOB,
			"Religin":request.payload.Religin,
			"knownLanguage":request.payload.knownLanguage,
			//Education Details
			"Class":request.payload.Class,
			"Degree":request.payload.Degree,
			"PG":request.payload.PG,
			"Diploma":request.payload.Diploma,
			"Course":request.payload.Course,
			"CareerObjective":request.payload.CareerObjective,
			"OtherDetails":request.payload.OtherDetails,
			// OtherDetails
			"ReferenceName":request.payload.ReferenceName,
			"ReferenceMobile":request.payload.ReferenceMobile,
			// Looking Overseas?
			"LookingOverseas": request.payload.LookingOverseas,
			// Please Tick If do you have current Work Details
			"NameofImpoloyer":request.payload.NameofImpoloyer,
			"PositionDesignation":request.payload.PositionDesignation,
			"MainJobCategory":request.payload.MainJobCategory,
			"Skills":request.payload.Skills,
			"Experience":request.payload.Experience,
			"currentSalary":request.payload.currentSalary,
			"ExpSalary":request.payload.ExpSalary,
			"PriferredShift":request.payload.PriferredShift,
			"PriferredJobDescription":request.payload.PriferredJobDescription,
			"PriferredLocation":request.payload.PriferredLocation,
			// If do you have past work Details
			"NameofImpoloyer1":request.payload.NameofImpoloyer1,
			"PositionDesignation1":request.payload.PositionDesignation1,
			"JobCategory1":request.payload.JobCategory1,
			"State1City1":request.payload.State1City1,
			"Experience1":request.payload.Experience1,
			"shift1":request.payload.shift1,
			"jobDescription1":request.payload.jobDescription1,
			"SalaryWithdrawn":request.payload.SalaryWithdrawn,
			"termsandcondition": request.payload.termsandcondition,
			"awsImageUrl": request.payload.awsImageUrl,
		    "awsResumeUrl": request.payload.awsResumeUrl
		});
		newResume.save()
		.then(function(response){
			return reply({
				StatusCode: 200,
				message: 'success',
				data: response
			});
		})
	}
	
},
{
	method: 'POST',
	path: '/android/post/job/images',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"post job images",
         notes:"post job images",
     },
	handler: function(request, h){
		let file =  request.payload.file;
    	uploadFileToS3(file, "JobsImageUrl", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		return h({
    			StatusCode: 200,
    			message: 'success',
    			awsImageUrl: fileLink 
    		})
    	});
	}
},
{
	method: 'POST',
	path: '/android/post/job-user',
	config: {
	//include this route in swagger documentation
		tags:['api'],
		description:"posting user jobs with image",
		notes:"in this route user can post there job with image",
		validate:{
            payload:{
            	"objectID": Joi.string(),
			    "jobType":Joi.string(),
			    "skills":Joi.string(),
			    "jobDescription":Joi.string(),
			    "aVacancy":Joi.string(),
			    "expiryDate": Joi.string(), //{ type: Date,required: true, default: Date.now },
			    "country":Joi.string(),
			    "state":Joi.string(),
			    "city":Joi.string(),
			    "jobArea":Joi.string(),
			    "pinCode":Joi.number(),
			    "jobAddress":Joi.string(),

			    //Professional Details
			    "salary":Joi.number(),
			    "experience":Joi.number(),
			    "shift":Joi.string(),
			    "gender":Joi.string(),
			    "educations":Joi.string(),
			    "knownLanguage":Joi.string(),

			    //Employer Details
			    "companyName":Joi.string(),
			    "nameOfRepresentative":Joi.string(),
			    "mobile":Joi.number(),
			    "landline":Joi.number(),
			    "email":Joi.string(),
			    "idCardNumber":Joi.string(),
			    "addressOfEmployer":Joi.string(),
			    "contactTiming":Joi.string(),
			    "lookingOverseas":Joi.string(),
			    "termsandcondition": Joi.boolean(),
			    "awsImageUrl": Joi.string(),
            }
        },
	}, 
	handler: (request, reply) => {
		var newJobs = new jobsModel({
			 //Job Details
		    "objectID": request.payload.objectID,
		    "JobID": "0",
		    "verifi": "In Active",
		    "jobType":request.payload.jobType,
		    "skills":request.payload.skills,
		    "jobDescription":request.payload.jobDescription,
		    "aVacancy":request.payload.aVacancy,
		    "expiryDate": request.payload.expiryDate, //{ type: Date,required: true, default: Date.now },
		    "country":request.payload.country,
		    "state":request.payload.state,
		    "city":request.payload.city,
		    "jobArea":request.payload.jobArea,
		    "pinCode":request.payload.pinCode,
		    "jobAddress":request.payload.jobAddress,

		    //Professional Details
		    "salary":request.payload.salary,
		    "experience":request.payload.experience,
		    "shift":request.payload.shift,
		    "gender":request.payload.gender,
		    "educations":request.payload.educations,
		    "knownLanguage":request.payload.knownLanguage,

		    //Employer Details
		    "companyName":request.payload.companyName,
		    "nameOfRepresentative":request.payload.nameOfRepresentative,
		    "mobile":request.payload.mobile,
		    "landline":request.payload.landline,
		    "email":request.payload.email,
		    "idCardNumber":request.payload.idCardNumber,
		    "addressOfEmployer":request.payload.addressOfEmployer,
		    "contactTiming":request.payload.contactTiming,
		    "lookingOverseas":request.payload.lookingOverseas,
		    "termsandcondition": request.payload.termsandcondition,
		    "awsImageUrl": request.payload.awsImageUrl,
		});
		newJobs.save()
		.then(function(response){
			return reply({
				StatusCode: 200,
				message: 'success',
				data: response
			})
		});
	}
	
},

// html render pages
{
	method: 'GET',
	path: '/android/companystory',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"company story",
         notes:"Get here Company Story",
     },
	handler: (request, h)=>{
		return h.view('companystory', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/wayofwork',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"post job images",
         notes:"post job images",
     },
	handler: (request, h)=>{
		return h.view('wayofwork', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/companyfeture',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Company Feture",
         notes:"post job images",
     },
	handler: (request, h)=>{
		return h.view('companyfeture', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/contectus',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Contect Us",
         notes:"Contect Us",
     },
	handler: (request, h)=>{
		return h.view('contectus', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/policyofcompany',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"policy of company",
         notes:"policy of company",
     },
	handler: (request, h)=>{
		return h.view('policyofcompany', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/termsandcondition',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"terms and condition",
         notes:"terms and condition",
     },
	handler: (request, h)=>{
		return h.view('termsandcondition', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/privacypolicy',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"privacypolicy",
         notes:"privacypolicy",
     },
	handler: (request, h)=>{
		return h.view('privacypolicy', null, {layout: 'layout4'})
	}
},
{
	method: 'GET',
	path: '/android/securegetwaypayment',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"securegetwaypayment",
         notes:"securegetwaypayment",
     },
	handler: (request, h)=>{
		return h.view('securegetway', null, {layout: 'layout4'})
	}
},
// end
{
	method: 'GET',
	path: '/android/user/get/posted/jobs',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting jobs of a particular user",
        notes:"getting jobs of a particular user demo id is string",
        validate:{
	        query:{
	        	id: Joi.string().required()
	        }
        },
	},
	handler: function(request, reply){
		async function getallDetails(){
			jobsModel.find({objectID: request.query.id})
			.then(function(jobs){
				return reply({
					StatusCode: 200,
					message: 'success',
					data: jobs
				})
			});
		}
		getallDetails();
	},
},
{
	method: 'GET',
	path: '/android/user/get/posted/resume',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user demo id is string",
        validate:{
        	query:{
        		id: Joi.string().required(),
        	}
        }
	},
	handler: function(request, reply){
		async function getallDetails(){
			ResumeModel.find({objectID: request.query.id})
			.then(function(resumes){
				return reply({
					StatusCode: 200,
					message: 'success',
					data: resumes
				})
			});
		}
		getallDetails();
	},
},
{
	method: 'GET',
	path: '/android/user/get/posted/service',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user demo id is string",
        validate:{
        	query:{
        		id: Joi.string().required()
        	}
        }
	},
	handler: function(request, reply){
		async function getallDetails(){
			ServiceModel.find({userobjectid: request.query.id})
			.then(function(services){
				return reply({
					StatusCode: 200,
					message: 'success',
					data: services
				})
			});
		}
		getallDetails();
	},
},

// right worker job and service
{
	method: 'GET',
	path: '/android/get/rightworkers/json',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting rsumes",
         notes:"Getting resumes for more resume put page number on count",
         validate:{
         	query:{
         		count: Joi.string()
         	}
         }
     },
	handler: (request, reply) =>{
		async function getallDetails(){
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		ResumeModel.find({verifi: 'Active'}).limit(20).skip(20 * request.query.count)
     		.then(function(allResume){
				for(var i = 0; i < allResume.length; i++){
					var str = allResume[i].Mobile;
					var str1 = allResume[i].AlternateNum;
					var str2 = allResume[i].EmailorMobile;

					if (allResume[i].Mobile == null) {
						continue
					}
					str = str.toString();
					str = str.slice(0, -4);
					str = parseInt(str);
					allResume[i].Mobile = str

					if (allResume[i].AlternateNum == null) {
						continue
					}
					str1 = str1.toString();
					str1 = str1.slice(0, -4);
					str1 = parseInt(str1);
					allResume[i].AlternateNum = str1
					
				}
				return reply({
					StatusCode: 200,
					message: 'success',
					pageNo: request.query.count,
					data: allResume
				})     			
     		})
     	}
     	getallDetails() 	
     }   
},
{
	method: 'GET',
	path: '/android/get/rightservice/json',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting service",
         notes:"in this route we are getting all Service",
         validate:{
         	query:{
         		count: Joi.string()
         	}
         }
     },
	handler: (request, reply) =>{
		async function getallDetails(){
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		ServiceModel.find({verifi: 'Active'}).limit(20).skip(20 * request.query.count)
     		.then(function(allService){
     			return reply({
					StatusCode: 200,
					message: 'success',
					pageNo: request.query.count,
					data: allService
				})
     		});
     	}
     	getallDetails() 	   
	}
},
{
	method: 'GET',
	path: '/android/get/rightjobs/json',
	config	: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"getting jobs",
         notes:"in this route we are getting all jobs",
         validate:{
         	query:{
         		count: Joi.string()
         	}
         }
     },
	handler: (request, reply) =>{
		async function getallDetails(){
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		jobsModel.find({verifi: 'Active'}).limit(20).skip(20 * request.query.count)
     		.then(function(allJobs){
     			return reply({
					StatusCode: 200,
					message: 'success',
					pageNo: request.query.count,
					data: allJobs
				})
     		});
     	}
     	getallDetails() 	   
	}
},
]
export default routes;

