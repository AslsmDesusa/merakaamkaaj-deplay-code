'use strict'

import axios from 'axios';
import jwt from 'jsonwebtoken';

// import bson.objectid from 'ObjectId';

const db = require('../database').db;
const Joi = require('joi');
const fs = require('fs');
const path = require('path')
const jobsModel = require('../models/postjob');
const ServiceModel = require('../models/service')
const ResumeModel = require('../models/postresume')
const UserModel = require('../models/users')
const swal = require('../node_modules/sweetalert')
const nodemailer = require("nodemailer");
const localStorage = require('node-localstorage')
const opn = require('opn');
const JobCategoryModel = require('../models/addjobcategory')
const ServiceModel1 = require('../models/addservice')
const AuthCookie = require('hapi-auth-cookie')
const async = require('async');
var ObjectID = require('mongodb').ObjectID
// var MongoDataTable = require('mongo-datatable');

// alert
import alert from 'alert-node'

import uploadFileToS3  from "./utils/uploadFileToS3"
import uploadResumeToS3  from "./utils/uploadResumeToS3"







const routes = [
// =============================================================================
// User Posted Details
{
		method: 'GET',
		path: '/user/forms',
		config:{
		//include this route in swagger documentation
		tags:['api'],
		description:"getting  admin form",
	    notes:"getting form",
	    auth:{
    		strategy: 'restricted',
	    }
    },
	handler: (request, h)=>{
		let authenticated_user = request.auth.credentials;
		JobCategoryModel.find()
		.then(function(response){
			ServiceModel1.find()
			.then(function(res){
				return h.view('form1', {jobcat: response, SerCat: res, userdata: authenticated_user}, {layout: "layout3"})
			})
		})
	}
},
{
	method: 'GET',
	path: '/user/get/posted/resume',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user",
        auth:{
	    	strategy: 'restricted',
	    }
	},
	handler: function(request, reply){
		async function getallDetails(){
			let authenticated_user = request.auth.credentials;
			let authenticated_u_m = authenticated_user._id;
			await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
			ResumeModel.find({objectID: authenticated_u_m})
			.then(function(resumes){
				ServiceModel.find({userobjectid: authenticated_u_m})
				.then(function(services){
					jobsModel.find({objectID: authenticated_u_m})
					.then(function(jobs){
						return reply.view('userposted', {resumedata: resumes, service: services, jobs: jobs, userdata: authenticated_user}, {layout: 'layout3'})
					});
				});
			});
		}
		getallDetails();
	},
},
{
    method:'POST',
    path:'/user/login',
    config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user",
        validate: {
         	payload:{
         		username:Joi.string(), 
         		password:Joi.string().required(),

			}
		},
    },
    handler: function(request, reply){
    	UserModel.findOne({emailid: request.payload.username, password: request.payload.password})
    	.then(function(user){
    		if (!user) {
    			UserModel.findOne({mobile: request.payload.username, password: request.payload.password})
    			.then(function(mobileuser){
    				if (!mobileuser) {
    					return reply.view('error', {message: 'User dose not exists please try with your correct email and password', errormessage: '404 error'})
    				} else if (mobileuser.Status == "SuperAdmin") {
    					let mobile = request.payload.username;
    					const token = jwt.sign({
    						mobile,
    						userid:mobileuser['_id'],
    					},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
    						algorithm: 'HS256',
    						expiresIn: '1h',
    					});
		            	reply.state('emailid', mobile)
		            	request.cookieAuth.set({ token });
		            	return reply.redirect('/admin/deshboard')
        			} else if (mobileuser.Status == "User") {
        				let mobile = request.payload.username;
        				const token = jwt.sign({
		            		mobile,
		            		userid:mobileuser['_id'],
		            	},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
		            		algorithm: 'HS256',
		            		expiresIn: '1h',
		            	});
		            	request.cookieAuth.set(mobileuser);
		            	return reply.redirect('/user/deshboard')
        			} else {
        				reply.view('error', {message: 'User dose not exists please try with your correct email and password', errormessage: '404 error'})
        			}
        		})
        	}else {
        		if (user.Status == "SuperAdmin") {
        			let emailid = request.payload.username;
        			const token = jwt.sign({
        				emailid,
        				userid:user['_id'],
        			},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
        				algorithm: 'HS256',
        				expiresIn: '1h',
        			});
        			reply.state('emailid', emailid)
        			request.cookieAuth.set({ token });
        			return reply.redirect('/admin/deshboard')
        		} else if (user.Status == "User") {
        			let emailid = request.payload.username;
        			const token = jwt.sign({
        				emailid,
        				userid:user['_id'],
        			},'vZiYpmTzqXMp8PpYXKwqc9ShQ1UhyAfy', {
        				algorithm: 'HS256',
        				expiresIn: '1h',
        			});
        			request.cookieAuth.set(user);
        			return reply.redirect('/user/deshboard')
        		}
        	}
        });
    }
},
{
		method: 'PUT',
		path: '/update/user/data',
		config:{
	    //include this route in swagger documentation
	    tags:['api'],
	    description:"update",
	    notes:"update user data",
	    auth:{
	    	strategy: 'restricted',
	    }
	},
	handler:(request, reply)=>{
		let authenticated_user = request.auth.credentials;
  		let _id = authenticated_user._id;
		var newData = { $set: 
			{
				mobile: request.payload.mobile,
				emailid: request.payload.emailid,
				address: request.payload.address,
				state: request.payload.state,
				city: request.payload.city,
				pincode: request.payload.pincode,
				gender: request.payload.gender,
			}
		};
		UserModel.findOneAndUpdate({_id: ObjectID(_id)}, newData)
		.then(function(response){
			UserModel.findOne({_id: ObjectID(_id)})
			.then(function(data){
				request.cookieAuth.set(data);
				alert('user data updated successfully')
				return reply('done')
			})
		});
	}

},
{
		method: 'POST',
		path: '/update/image',
		config:{
	    //include this route in swagger documentation
	    tags:['api'],
	    description:"update",
	    notes:"update user data",
	    auth:{
	    	strategy: 'restricted',
	    }
	},
	handler:(request, reply)=>{
		let authenticated_user = request.auth.credentials;
  		let _id = authenticated_user._id;
  		var file = request.payload.file
		uploadFileToS3(file, "userImage", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		var newData = { $set: 
				{
					picture: fileLink,
				}
			};
    		UserModel.findOneAndUpdate({_id: ObjectID(_id)}, newData)
    		.then(function(response){
    			request.cookieAuth.set(response);
    			alert('user image uploaded successfully')
				return reply.redirect('/user/user-account')
    		})
    	});
	}

},
{
		method: 'PUT',
		path: '/change/user/password',
		config:{
	    //include this route in swagger documentation
	    tags:['api'],
	    description:"update",
	    notes:"update user data",
	    auth:{
	    	strategy: 'restricted',
	    }
	},
	handler:(request, reply)=>{
		var newData = { $set: 
			{
				password: request.payload.password,
			}
		};
		UserModel.findOneAndUpdate({_id: ObjectID(request.query._id)})
		.then(function(response){
			return reply('password changed successfully')
		});
	}

},
{
		method: 'GET',
		path: '/user/deshboard',
		config:{
	    //include this route in swagger documentation
	    tags:['api'],
	    description:"getting details of a particular user",
	    notes:"getting details of particular user",
	    auth:{
	    	strategy: 'restricted',
	    }
	},
	handler:(request, h)=>{
  		let authenticated_user = request.auth.credentials;
  		let email_id = authenticated_user.emailid;
        UserModel.findOne({emailid: email_id}, function(err, user){
        	if (err) {
        		throw err
        	}else{
        		return h.view('deshboard1', {userdata: user, message: '“It is a great honour to have you in our team. Congratulations and welcome.”',}, {layout: 'layout3'})
        		
        	}
        });
	}

},
{
	method: 'POST',
	path: '/Registere/user',
	config: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Registere user details",
         notes:"in this route we can post details of a user",
         validate: {
         	payload:{
         		fullname:Joi.string().required(),
         		gender:Joi.string().required(),
         		emailid:Joi.string().required(),
         		password:Joi.string().required(),
    			lookingfor:Joi.string().required(),

			}
		},
	},
	handler: (request, reply) => {
		var newUser = new UserModel(request.payload);
		newUser.save(function (err, data){
			if (err) {
				throw err;
			}else {
				reply({
					statusCode: 200,
					message: "new user successfully Registered",
					data: data
				});
			}
		});
	}
	
},
{
	method: 'POST',
	path: '/user/post/resume',
	config: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Posting resume",
         notes:"in this route we can post resume",
         auth:{
         	strategy: 'restricted',
         },
	},
	handler: (request, reply) => {
		let file =  request.payload.file;
		let file1 =  request.payload.resume;
		let authenticated_user = request.auth.credentials;
		var newResume = new ResumeModel({
			"objectID": authenticated_user._id,
			"pwid": "0", 
			"verifi": "In Active",
			"JobCat": request.payload.JobCat,
			"Name":request.payload.Name,
			"Mobile": request.payload.Mobile,
			"AlternateNum":request.payload.AlternateNum,
			"EmailorMobile": request.payload.EmailorMobile, 
			"AdharNo":request.payload.AdharNo,
			"Country":request.payload.Country,
			"State":request.payload.State,
			"City":request.payload.City,
			"Pincode":request.payload.Pincode,
			"Address":request.payload.Address,
			//About My Self
			"Gender":request.payload.Gender,
			"DOB":request.payload.DOB,
			"Religin":request.payload.Religin,
			"knownLanguage":request.payload.knownLanguage,
			//Education Details
			"Class":request.payload.Class,
			"Degree":request.payload.Degree,
			"PG":request.payload.PG,
			"Diploma":request.payload.Diploma,
			"Course":request.payload.Course,
			"CareerObjective":request.payload.CareerObjective,
			"OtherDetails":request.payload.OtherDetails,
			// OtherDetails
			"ReferenceName":request.payload.ReferenceName,
			"ReferenceMobile":request.payload.ReferenceMobile,
			// Looking Overseas?
			"LookingOverseas": request.payload.LookingOverseas,
			// Please Tick If do you have current Work Details
			"NameofImpoloyer":request.payload.NameofImpoloyer,
			"PositionDesignation":request.payload.PositionDesignation,
			"MainJobCategory":request.payload.MainJobCategory,
			"Skills":request.payload.Skills,
			"Experience":request.payload.Experience,
			"currentSalary":request.payload.currentSalary,
			"ExpSalary":request.payload.ExpSalary,
			"PriferredShift":request.payload.PriferredShift,
			"PriferredJobDescription":request.payload.PriferredJobDescription,
			"PriferredLocation":request.payload.PriferredLocation,
			// If do you have past work Details
			"NameofImpoloyer1":request.payload.NameofImpoloyer1,
			"PositionDesignation1":request.payload.PositionDesignation1,
			"JobCategory1":request.payload.JobCategory1,
			"State1City1":request.payload.State1City1,
			"Experience1":request.payload.Experience1,
			"shift1":request.payload.shift1,
			"jobDescription1":request.payload.jobDescription1,
			"SalaryWithdrawn":request.payload.SalaryWithdrawn,
			"termsandcondition": request.payload.termsandcondition,
			"awsImageUrl": "",
			"awsResumeUrl": "",
		});
		uploadFileToS3(file, "ResumeImageUrl", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		console.log(fileLink)
    		newResume.awsImageUrl = fileLink
    	})
    	uploadResumeToS3(file1, "ResumeImageUrl", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		console.log(fileLink)
    		newResume.awsResumeUrl = fileLink
			newResume.save()
			.then(function(res){
				alert('your resume saved successfully in your account this resume is not active at this time before proceed we will  review your resume and get back to you as soon as possible');
				return reply.redirect('/user/forms');
			})
    	})	
	}
	
},
{
	method: 'POST',
	path: '/user/post/service',
	config: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Posting user details",
         notes:"in this route we can post details of a user service",
         auth:{
         	strategy: 'restricted',
         },
	},
	handler: (request, reply) => {
		let file =  request.payload.file;
		let authenticated_user = request.auth.credentials;
        var uuid = {}
    	uploadFileToS3(file, "ServiceImageUrl", "image",  'kaamkaajcom')
    	.then(({ fileLink }) => {
    		var service = new ServiceModel({
			 	//service Details
			    "userobjectid": authenticated_user._id,
			    "serviceid": "0",
			    "verifi": request.payload.verifi,
			    "TypeOfService": request.payload.TypeOfService,
				"Specification": request.payload.specification,
				"ProvideServices": request.payload.ProvideServices,
				"ProviderRegistered": request.payload.ProviderRegistered,
				"RegisteredExpiry": request.payload.RegisteredExpiry,
				"Country": request.payload.Country,
				"State": request.payload.State,
				"City": request.payload.City,
				"Area": request.payload.Area,
				"Agency": request.payload.Agency,
				"Representative": request.payload.Representative,
				"MobileNumber": request.payload.MobileNumber,
				"LandNumber": request.payload.LandNumber,
				"Timing": request.payload.Timing,
				"aadharcard": request.payload.aadharcard,
				"website": request.payload.website,
				"emailMobile": request.payload.emailMobile,
				"address": request.payload.address,
				"pincode": request.payload.pincode,
				"information": request.payload.information,
				"payment": request.payload.payment,
				"termsandcondition": request.payload.termsandcondition,
				"awsImageUrl": fileLink,
			});
			service.save()
			.then(function(res){
				alert('service saved successfully in your account this service is not active at this time before proceed we will  review your service and get back to you as soon as possible');
				return reply.redirect('/user/forms');

			})
    	});
		
	}
},
{
	method: 'POST',
	path: '/user/post/job',
	config: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"Posting user details",
         notes:"in this route we can post details of a user jobs",
         auth:{
         	strategy: 'restricted',
         },
	},
	handler: (request, reply) => {
		var data = request.auth.credentials
		var newJobs = new jobsModel({
			 //Job Details
		    "objectID": data._id,
		    "JobID":"0",
		    "verifi": "In Active",
		    "jobType":request.payload.jobType,
		    "skills":request.payload.skills,
		    "jobDescription":request.payload.jobDescription,
		    "aVacancy":request.payload.aVacancy,
		    "expiryDate": request.payload.expiryDate, //{ type: Date,required: true, default: Date.now },
		    "country":request.payload.country,
		    "state":request.payload.state,
		    "city":request.payload.city,
		    "jobArea":request.payload.jobArea,
		    "pinCode":request.payload.pinCode,
		    "jobAddress":request.payload.jobAddress,

		    //Professional Details
		    "salary":request.payload.salary,
		    "experience":request.payload.experience,
		    "shift":request.payload.shift,
		    "gender":request.payload.gender,
		    "educations":request.payload.educations,
		    "knownLanguage":request.payload.knownLanguage,

		    //Employer Details
		    "companyName":request.payload.companyName,
		    "nameOfRepresentative":request.payload.nameOfRepresentative,
		    "mobile":request.payload.mobile,
		    "landline":request.payload.landline,
		    "email":request.payload.email,
		    "idCardNumber":request.payload.idCardNumber,
		    "addressOfEmployer":request.payload.addressOfEmployer,
		    "contactTiming":request.payload.contactTiming,
		    "lookingOverseas":request.payload.lookingOverseas,
		    "termsandcondition": request.payload.termsandcondition,
		    "awsImageUrl": data.picture
		});
		newJobs.save()
		.then(function(res){
			alert('job saved successfully in your account this job is not active at this time before proceed we will review your job and get back to you as soon as possible')
			return reply.redirect('/user/forms')		
		})
	}
	 
},
{
	method: 'POST',
	path: '/send/otp',
	handler: function(request, reply){
		var otp = Math.floor(Math.random() * 90000) + 10000;
		var newUser = new UserModel({
			"firstname": request.payload.firstname,
			"lastname": request.payload.lastname,
			"mobile": request.payload.mobile,
			"emailid": request.payload.emailid,
			"password": request.payload.password,
			"address": request.payload.address,
			"state": request.payload.state,
			"city": request.payload.city,
			"pincode": request.payload.pincode,
			"gender": request.payload.gender,
			"lookingfor": request.payload.lookingfor,
			"Status": "User",
			"picture": "N/A",
			"isVerified": true,
			"otp": otp
		});
		request.cookieAuth.set(newUser);
		UserModel.findOne({mobile: request.payload.mobile})
		.then(function(result){
			if (!result) {
				let otpmessage = 'merakaamkaaj.com \n Your OTP is:'+otp
				axios.request('http://zapsms.co.in/vendorsms/pushsms.aspx?user=merakaamkaaj&password=merakaamkaaj&msisdn='+request.payload.mobile+'&sid=MERAKK&msg='+otpmessage+'&fl=1&gwid=2')
				.then(function(res){
					return reply.view('confirmemail', {message: "We've sent an OTP to "+request.payload.mobile+". If this is a valid Phone Number, you should receive an OTP within the next few minutes.", successmessage: '200'})
				})
				.catch(error => {
					console.log(error);
				});
			}else{
				return reply.view('error', {errormessage: '400', message:'User already Exist Please Try with your another mobile number', message3: 'LOGIN'})
			}
		})
	}
},
{
    method:'POST',
    path:'/verifing/phone/number',
    config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user",
        auth:{
    		strategy: 'restricted',
	    }
    },
    handler: function(request, reply){
      	var headers = request.auth.credentials;
  		let otp = headers.otp;	
  		var newUser = new UserModel(headers);
  		if (otp == request.payload.otp) {
  			newUser.save(function(err, data){
  				if (err) {
  					return reply.view('error', {message: 'User Already Exists Please Try With Another Email or Phone Number', errormessage: '400'})
  				}else{
  					let success = 'Registered Successfully Thanks To Be A Member Of Merakaamkaaj.com your ID: '+headers.emailid+' Your Password: '+headers.password+''
  					axios.request('http://zapsms.co.in/vendorsms/pushsms.aspx?user=merakaamkaaj&password=merakaamkaaj&msisdn='+headers.mobile+'&sid=MERAKK&msg='+success+'&fl=0&gwid=2')
  					.then(reply => {
  						console.log("messages sent to your number")
  					})
  					.catch(error => {
  						console.log(error);
  					});
  					return reply.view('error', {errormessage: '200', message:'Your Profile Has Successfully Made By Mera Kaam kaaj Please Login First', message3: 'LOGIN'})
  				}
  			})
  		}else{
  			return reply.view('error', {message: "Wrong OTP Please Try With Correct OTP PIN if you Didn't Get Any Code Please Go Back And Try Again", errormessage: '400'})
  		}
    }
},
{
	method:  'POST',
	path: '/sending/mail',
	config: {
    	payload: {
    		output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },
    },
	handler: (request, h) => {
		// var userdata = {}
		var uuid = {}
		var otp = Math.floor(Math.random() * 90000) + 10000;
		var transporter = nodemailer.createTransport({
			host: 'smtp.gmail.com',
			port: 587,
			secure: false, // true for 465, false for other ports
			auth: {
			    user: 'aslam17@navgurukul.org', // generated ethereal user
			    pass: 'aslam#desusa' // generated ethereal password
			}
			});
			// setup email data with unicode symbols
	    	var mailOptions = {
		        from: '"Mera kaam Kaaj " <aslam17@navgurukul.org>', // sender address
		        to: request.payload.emailid,  // list of receivers
		        subject: 'Mera Kaamk Kaaj email verification message', // Subject line
		        text: 'Hello world', // plain text body
		        html: '<div style="width: 100%"><span style="background: #f26136; font-size: 22px; color: white; padding:16px; display: flex; justify-content: center;">Dear Mera Kaam Kaaj.Com  User</span><br><br>We received a request for registration in merakaamkaaj.com through your email address. Your verification CODE is :<br><br> '+otp+'<br><br> Please note for registration. If you did not request this, it is possible that someone else is trying to access the Account in merakaamkaaj.com. <b>Do not forward or give this code to anyone</b>.<br><br>You received this message because this email address is listed for registration in merakaamkaaj.com. Please click <a href="/">here</a> to ACCESS your account in merakaamkaaj.com.<br><br>Sincerely yours,<br><br>The merakaamkaaj.com tea,</div>' // html body
		    };
		// var newUser = new UserModel(request.payload,);
		var newUser = new UserModel({
				"firstname": request.payload.firstname,
				"lastname": request.payload.lastname,
				"mobile": request.payload.mobile,
				"emailid": request.payload.emailid,
				"password": request.payload.password,
				"address": request.payload.address,
				"state": request.payload.state,
				"city": request.payload.city,
				"pincode": request.payload.pincode,
				"lookingfor": request.payload.lookingfor,
				"otp": otp
			});
		UserModel.find({emailid: request.payload.emailid},function(err, docs){
			if (docs.length) {
				return h.view('error', {message: request.payload.emailid+' Already Exists Please Try With Another Email', errormessage: '409'})
			}else{
				newUser.save(function(err, data){
					if (err) {
						throw err
					}else{
						uuid=data;
						var dataa = request.payload;
			            if (dataa.image) {
			                var name = uuid._id + ".jpg";
			                const __dirname = '../completemerakaamkaaj/merakaamkaaj/uploads'
			                var path = __dirname + "/profileImage/" + name;
			                var file = fs.createWriteStream(path);

			                file.on('error', function (err) { 
			                    console.error('error') 
			                });
			                dataa.image.pipe(file);
			                dataa.image.on('end', function (err) {
			                    var ret = {
			                        filename: uuid._id + ".jpg",
			                        headers: dataa.image.hapi.headers

			                    }
			                });
				        }
						transporter.sendMail(mailOptions, (err, info) => {
							if (err) {
								throw err;
								
							}else{
								h.state('emailid', data.emailid)
								return h.view('confirmemail', {message: "We've sent an email to "+request.payload.emailid+". If this is a valid email address, you should receive an email with your username(s) within the next few minutes.", successmessage: '200'})
							}
						});
					}
				})
			}
		});
	}
},
{
    method:'POST',
    path:'/verifing/email/address',
    config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting details of a particular user",
        notes:"getting details of particular user",
    },
    handler: function(request, reply){
      	var headers = request.headers.cookie.split('=')
        var email = headers[1].split(';')[0]
        var params = email
        UserModel.find({'emailid': params, 'otp' : request.payload.otp}, function(err, data){
            if(err){
                return reply.view('error', {message: 'Wrong OTP Please Fill Right OTP', errormessage: '400'})
            } else {
                return reply.view('error', {errormessage: '200', message:'Your Profile successfully Made By Mera Kaam kaaj Please login first', message3: 'LOGIN'})
            }
        });
    }
},
// ============user getting posted details and getting form ==================================
{
	method: 'GET',
	path: '/search/right/worker',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting admin form",
        notes:"getting form",
    },
	handler: (request, reply) =>{
		// return reply('dsf')
		async function getallDetails(){
     		var worker;
			var jobcat;
			var query = {$and:[{JobCat:{$regex: request.query.JobCat, $options: 'i'}}]}
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		ResumeModel.find(query).limit(20).skip(20 * request.query.count)
     		.then(function(allResume){
				 for(var i = 0; i < allResume.length; i++){
					var str = allResume[i].Mobile;
					var str1 = allResume[i].AlternateNum;
					var str2 = allResume[i].EmailorMobile;

					if (allResume[i].Mobile == null) {
						continue
					}
					str = str.toString();
					str = str.slice(0, -4);
					str = parseInt(str);
					allResume[i].Mobile = str

					if (allResume[i].AlternateNum == null) {
						continue
					}
					str1 = str1.toString();
					str1 = str1.slice(0, -4);
					str1 = parseInt(str1);
					allResume[i].AlternateNum = str1
					
				}
     			worker = allResume
     			JobCategoryModel.find({})
     			.then(function(allJobCategory){
     				jobcat = allJobCategory
	     			return reply.view('SearchRightWorker', {allResume : allResume, jobcategorys: jobcat})

	     			// return reply({allResume : worker, jobcategorys: jobcat})
     			});
     		});
     	}
     	getallDetails();
	}
},
{
	method: 'GET',
	path: '/search/right/worker/json',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting admin form",
        notes:"getting form",
    },
	handler: (request, reply) =>{
		// return reply('dsf')
		async function getallDetails(){
			var query = {$and:[{JobCat:{$regex: request.query.JobCat, $options: 'i'}}]}
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		ResumeModel.find(query).limit(20).skip(20 * request.query.count)
     		.then(function(allResume){
				 for(var i = 0; i < allResume.length; i++){
					var str = allResume[i].Mobile;
					var str1 = allResume[i].AlternateNum;
					var str2 = allResume[i].EmailorMobile;

					if (allResume[i].Mobile == null) {
						continue
					}
					str = str.toString();
					str = str.slice(0, -4);
					str = parseInt(str);
					allResume[i].Mobile = str

					if (allResume[i].AlternateNum == null) {
						continue
					}
					str1 = str1.toString();
					str1 = str1.slice(0, -4);
					str1 = parseInt(str1);
					allResume[i].AlternateNum = str1
					
				}
				return reply(allResume)
     		});
     	}
     	getallDetails();
	}
},
{
	method: 'GET',
	path: '/search/right/jobs',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"Getting Jobs from pic our workers",
        notes:"Getting Jobs from pic our worker",
    },
	handler: (request, reply) =>{
		// return reply('dsf')
		async function getallDetails(){
     		var jobs;
			var jobcat;
			var query = {$and:[{verifi: "Active"}, {jobType:{$regex: request.query.jobType, $options: 'i'}}]}
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		jobsModel.find(query)
     		.then(function(allService){
     			jobs = allService
     			JobCategoryModel.find({})
     			.then(function(allJobCategory){
     				jobcat = allJobCategory
	     			return reply.view('SearchRightJobs', {allJobs : jobs, jobcategorys: jobcat})

	     			// return reply({allResume : worker, jobcategorys: jobcat})
     			});
     		});
     	}
     	getallDetails();
	}
},
{
	method: 'GET',
	path: '/search/right/services',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"Getting Jobs from pic our workers",
        notes:"Getting Jobs from pic our worker",
    },
	handler: (request, reply) =>{
		// return reply('dsf')
		async function getallDetails(){
     		var service;
			var serCat;
			var query = {$and:[{verifi: 'Active'}, {TypeOfService:{$regex: request.query.TypeOfService, $options: 'i'}}]}
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		ServiceModel.find(query)
     		.then(function(allService){
     			service = allService
     			ServiceModel1.find({})
     			.then(function(allserCategory){
     				serCat = allserCategory
	     			return reply.view('searchRightService', {allService : service, services: serCat})
	     			// return reply({allService : service, services: serCat})
     			});
     		});
     	}
     	getallDetails();
	}
},
// {
// 	method: 'GET',
// 	path: '/search/right/worker',
// 	config:{
//         //include this route in swagger documentation
//         tags:['api'],
//         description:"getting admin form",
//         notes:"getting form",
//     },
// 	handler: (request, reply) =>{
// 		// return reply('dsf')
// 		async function getallDetails(){
//      		var worker;
// 			var jobcat;
// 			var query = {$and:[{TypeOfService:{$regex: request.query.TypeOfService, $options: 'i'}}]}
//      		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
//      		ServiceModel.find(query)
//      		.then(function(allService){
//      			worker = allworker
//      			JobCategoryModel.find({})
//      			.then(function(allJobCategory){
//      				jobcat = allJobCategory
// 	     			return reply.view('SearchRightWorker', {allResume : worker, jobcategorys: jobcat})

// 	     			// return reply({allResume : worker, jobcategorys: jobcat})
//      			});
//      		});
//      	}
//      	getallDetails();
// 	}
// },
{
	method: 'GET',
	path: '/world/{worker}',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"getting  admin form",
        notes:"getting form",
        validate:{
            params:{
                worker:Joi.string()
            }
        },
    },
	handler: function(request, reply){
		// return reply('dsf')
		async function getallDetails(){
     		var worker;
			var jobcat;
     		await new Promise((resolve, reject) => setTimeout(() => resolve(), 1000));
     		ResumeModel.find(query)
     		.then(function(allworker){
     			worker = allworker
     			JobCategoryModel.find()
     			.then(function(allJobCategory){
     				jobcat = allJobCategory
	     			return reply.view('searchRightService', {allResume : worker, jobcategorys: jobcat})
     			});
     		});
     	}
     	getallDetails();
	}
},
{
	method: 'GET',
	path: '/job/change/status/to/verify/unverify',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"admin can change the status of any data",
        notes:"admin can cahnge the status of any data which is verify of not",
    },
	handler: function(request, reply){
		var unverify = ({
			verifi: "In Active"
		});
		var varify = ({
			verifi: "Active"
		});
		jobsModel.findOne({_id: ObjectID(request.query._id)}, function(err, result){
			if (result.verifi == "Active") {
				jobsModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, unverify, function(err, data){
					if (err) {
						throw err
					}else{
						reply({message: "Successfully In Active this it will not show on home"})
					}
				})
			}else if (result.verifi == "In Active") {
				jobsModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, varify, function(err, data){
					if (err) {
						throw err
					}else{
						reply({message:'Successfully Active Job it now it will show on home'})
					}
				})
			}
		})
	}
},
{
	method: 'GET',
	path: '/change/status/to/verify/unverify',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"admin can change the status of any data",
        notes:"admin can cahnge the status of any data which is verify of not",
    },
	handler: function(request, reply){
		var unverify = ({
			verifi: "In Active"
		});
		var varify = ({
			verifi: "Active"
		});
		ServiceModel.findOne({_id: ObjectID(request.query._id)}, function(err, result){
			if (result.verifi == "Active") {
				ServiceModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, unverify, function(err, data){
					if (err) {
						throw err
					}else{
						reply({message: "Successfully In Active Service"})
					}
				})
			}else if (result.verifi == "In Active") {
				ServiceModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, varify, function(err, data){
					if (err) {
						throw err
					}else{
						reply({message:'Successfully Active Service'})
					}
				})
			}
		})
	}
},
{
	method: 'GET',
	path: '/resume/change/status/to/verify/unverify',
	config:{
        //include this route in swagger documentation
        tags:['api'],
        description:"admin can change the status of any data",
        notes:"admin can cahnge the status of any data which is verify of not",
    },
	handler: function(request, reply){
		var unverify = ({
			verifi: "In Active"
		});
		var varify = ({
			verifi: "Active"
		});
		ResumeModel.findOne({_id: ObjectID(request.query._id)}, function(err, result){
			if (result.verifi == "Active") {
				ResumeModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, unverify, function(err, data){
					if (err) {
						throw err
					}else{
						reply({message: "Successfully In Active Service"})
					}
				})
			}else if (result.verifi == "In Active") {
				ResumeModel.findOneAndUpdate({_id: ObjectID(request.query._id)}, varify, function(err, data){
					if (err) {
						throw err
					}else{
						reply({message:'Successfully Active Service'})
					}
				})
			}
		})
	}
},
{
	method: 'GET',
	path: '/user/data/json',
	config:{
		auth:{
			strategy: 'restricted'
		},
	},
	handler: function(request, reply){
		UserModel.findOne({_id: ObjectID(request.query._id)})
		.then(function(res){
			return reply(res)
		})
	}
},
]
export default routes;
 
