import Hapi from 'hapi';
const UserModel = require('../models/users')

const routes = [
{
	method: 'GET',
	path: '/make/payment',
	config: {
		 //include this route in swagger documentation
		 tags:['api'],
		 description:"payment getway but still not working we are working on it",
         notes:"payment getway but still not working we are working on it",
         auth:{
         	strategy: 'restricted',
         },
	},
	handler: function(request, reply){
		let authenticated_user = request.auth.credentials;
  		let email_id = authenticated_user.emailid;
        UserModel.findOne({emailid: email_id}, function(err, user){
        	if (err) {
        		throw err
        	}else{
        		return reply.view('payment', {userdata: user},{layout: 'layout3'})
        		
        	}
        });
	}
},
]
export default routes;
