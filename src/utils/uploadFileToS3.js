const AWS = require('aws-sdk');

const generateUID = () =>{
    // I generate the UID from two parts here
    // to ensure the random number provide enough bits.
    let firstPart = ((Math.random() * 46656) | 0).toString(36);
    let secondPart = ((Math.random() * 46656) | 0).toString(36);
    firstPart = ("000" + firstPart).slice(-3);
    secondPart = ("000" + secondPart).slice(-3);
    return firstPart + secondPart;
}

const uploadFileToS3 = (file, folder, content,  bucket='kaamkaajcom') => {
  // content would have like image/pdf/doc/xlsx/csv

  let s3, fileExtension, fileName, params;

  //configuring the AWS environment
  AWS.config.update({
    accessKeyId: "AKIAJ6QSP7PVS2RXZTDQ",
    secretAccessKey: "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"
  });


  s3 = new AWS.S3();
  // file data
  // fileExtension = file.join(".")[file.join(".").length - 1];
  fileName = `${generateUID()}.png`
  //configuring parameters
  params = {
    Bucket: bucket,
    Body: file,
    Key: `${folder}/${fileName}`,
    ContentType: `${content}/png`,
    ACL:"public-read"
  };

  // return a promises containing the link
  return s3.upload(params).promise()
            .then((data)=> {
              return Promise.resolve({
                fileLink: `https://s3.eu-west-2.amazonaws.com/${bucket}/${folder}/${fileName}`
              })
            })
            .catch(error => {
              console.error(error);
              throw error;
            })
}

export default uploadFileToS3

